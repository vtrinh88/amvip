//
//  ViewController.mm
//  AMVIP
//
//  Created by Viet Trinh on 2/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import "ViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <opencv2/opencv.hpp>
#import <opencv2/videoio/cap_ios.h>
#import <opencv2/highgui/highgui.hpp>
#import <opencv2/imgproc/imgproc.hpp>
#import <stdio.h>
#import <stdlib.h>
#import <iostream>
#import "HandDetector.h"
#import "UIImage+OpenCV.h"
#import "BTDevAvailability.h"
#import "BTDevDiscovery.h"
#import "ManipulatorMeasurement.h"
#import "Manipulator.h"
using namespace cv;
using namespace std;

@interface ViewController () <CvVideoCameraDelegate, UIImagePickerControllerDelegate>
// Properties for hand detection mechanism
@property (weak, nonatomic) IBOutlet UIImageView *cameraView;
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *trackingView;
@property (weak, nonatomic) IBOutlet UIButton *rescanButton;
@property (weak, nonatomic) IBOutlet UILabel *numFingerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *panelView;
@property (strong, nonatomic) CvVideoCamera* camera;
@property (nonatomic) HandDetector* handDetector;
@property (strong, nonatomic) NSTimer* waitHandTimer;
@property (strong, nonatomic) NSTimer* trackingTimer;

// Properties for BLE communication mechanism
@property (weak, nonatomic) IBOutlet UIImageView *BTImage;
@property (strong, nonatomic) IBOutlet UILabel *BTConnectivityLabel;
@property (strong, nonatomic) NSTimer *TXDelayTimer;
@property (nonatomic) BOOL enableTX;

// Properties for Manipulator
@property (strong, nonatomic) Manipulator* manipulator;
@end

@implementation ViewController

#pragma mark - ViewLifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupHandDetector];
    [self setupPanelView];
    self.BTImage.image = [UIImage imageNamed:@"bluetooth_icon"];
    self.enableTX = YES;
    self.manipulator = [[Manipulator alloc] initWithL1:ML1 L2:ML2 L3:ML3];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(connectivityNotification:)
                                                 name:BT_SERVICE_STATUS_CHANGE
                                               object:nil];
    [BTDevDiscovery sharedInstance];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (![self deviceHasCamera])
        [self popupMessage:@"Your device does not have a camera" withTitle:@"AMVIP"];
    else
        [self setupCamera];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:BT_SERVICE_STATUS_CHANGE
                                                  object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self stopTXDelayTimer];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.camera stop];
    self.camera = nil;
    self.trackingView = nil;
}

-(void)setupPanelView{
    [self.panelView.layer setCornerRadius:10.0];
    [self.panelView setBackgroundColor:[UIColor grayColor]];
    [self.panelView setAlpha:0.8];
    [self.panelView setClipsToBounds:YES];
    [self.panelView setHidden:YES];
    
    [self.trackingView.layer setCornerRadius:5.0];
    [self.trackingView.layer setBorderColor:[UIColor blueColor].CGColor];
    [self.trackingView.layer setBorderWidth:2.0];
    [self.trackingView setClipsToBounds:YES];
    [self.trackingView setHidden:YES];
    
    [self.numFingerLabel setText:@"# Fingers: 0"];
    [self.numFingerLabel setHidden:YES];
    
    [self.rescanButton setHidden:YES];
}

-(void)setupCamera{
    self.camera = [[CvVideoCamera alloc] initWithParentView:self.cameraView];
    self.camera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    self.camera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
    self.camera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationLandscapeLeft;
    self.camera.defaultFPS = 30;
    self.camera.grayscaleMode = NO;
    self.camera.delegate = self;
    [self.camera start];
}

-(void)setupHandDetector{
    if (!_handDetector) {
        _handDetector = new HandDetector();
    }
    self.waitHandTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0/20.0)
                                                          target:self
                                                        selector:@selector(dismissInstruction:)
                                                        userInfo:nil
                                                         repeats:YES];
    
    self.trackingTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0/20.0)
                                                                target:self
                                                              selector:@selector(tracking:)
                                                              userInfo:nil
                                                            repeats:YES];
}

- (IBAction)rescan {
    self.handDetector = nil;
    self.waitHandTimer = nil;
    self.trackingTimer = nil;
    [self.instructionLabel setHidden:NO];
    [self hidePanel:YES];
    [self setupHandDetector];
}

-(void)hidePanel:(bool)isHidden{
    [self.panelView setHidden:isHidden];
    [self.trackingView setHidden:isHidden];
    [self.numFingerLabel setHidden:isHidden];
    [self.rescanButton setHidden:isHidden];
}

#pragma mark - UIAlertController
-(void)popupMessage:(NSString*)message withTitle:(NSString*)title{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alertAction = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _action) {
                                                            [alertController dismissViewControllerAnimated:YES
                                                                                                completion:nil];
                                                        }];
    [alertController addAction:alertAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate
-(BOOL)deviceHasCamera{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        NSArray* availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        if ([availableMediaTypes containsObject:(NSString*)kUTTypeImage]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - NSTimerDelegate
-(void)tracking:(NSTimer*)timer{
    if (self.handDetector->isBWImageReady()){
        int numberOfFingers = self.handDetector->numberOfFingers();
        [self kinematicsFor:numberOfFingers];
        self.trackingView.image = [UIImage fromCVMat:self.handDetector->getImageBW()];
        self.numFingerLabel.text = [NSString stringWithFormat:@"# Fingers: %d",numberOfFingers];
        self.numFingerLabel.textColor = [UIColor whiteColor];
        [self hidePanel:NO];
    }
}

-(void)dismissInstruction:(NSTimer*)timer{
    if (!self.handDetector->isWaitingForHand()) {
        self.instructionLabel.hidden = YES;
        [self.waitHandTimer invalidate];
    }
}

-(void)kinematicsFor:(int)number{
    if (!_manipulator) {
        _manipulator = [[Manipulator alloc] initWithL1:ML1 L2:ML2 L3:ML3];
    }
    NSArray* angles = [self.manipulator IKJointAnglesFor:number];
    if ([angles count] == 3) {
        float baseAngle = [[angles objectAtIndex:0] floatValue];
        float shoulderAngle = [[angles objectAtIndex:1] floatValue];
        float elbowAngle = [[angles objectAtIndex:2] floatValue];
        NSString* data = @"";
        data = [[[data stringByAppendingString:[NSString stringWithFormat:@"B%f-",baseAngle]]
                 stringByAppendingString:[NSString stringWithFormat:@"S%f-",shoulderAngle]]
                stringByAppendingString:[NSString stringWithFormat:@"E%f",elbowAngle]];
        NSLog(@"Data: %@",data);
        //[self sendData:data];
    }
}

#pragma mark - CvVideoCameraDelegate
-(void)processImage:(cv::Mat &)image{
    self.handDetector->scanImage(image);
}

#pragma mark - BLETXActions
- (void)stopTXDelayTimer{
    if (!self.TXDelayTimer) {
        return;
    }
    else{
        [self.TXDelayTimer invalidate];
        self.TXDelayTimer = nil;
    }
}

- (void)elapseTXDelayTimer{
    self.enableTX = YES;
    [self stopTXDelayTimer];
}

-(void)sendData:(NSString* )data{
    static NSString* lastTransmitedData = @"255";
    if (!self.enableTX || [data isEqualToString:lastTransmitedData] || [data isEqualToString:@""]) {
        NSLog(@"Couldn't transmit data. Could be no transmittion allowed, duplicate transmitted data, or invalid data");
        return;
    }
    
    if ([BTDevDiscovery sharedInstance].btService) {
        [[BTDevDiscovery sharedInstance].btService transmitData:data];
        lastTransmitedData = data;
        NSLog(@"Transmitted %@ successfully", data);
        self.enableTX = NO;
        if (!self.TXDelayTimer) {
            self.TXDelayTimer = [NSTimer scheduledTimerWithTimeInterval:0.25
                                                                 target:self
                                                               selector:@selector(elapseTXDelayTimer)
                                                               userInfo:nil
                                                                repeats:NO];
        }
    }
}

-(void)connectivityNotification:(NSNotification*)notification{
    BOOL isPaired = [notification.userInfo[@"isPaired"] boolValue];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (isPaired) {
            self.BTConnectivityLabel.text = isPaired ? @"Paired" : @"Disconnect";
        }
    });
}

@end
