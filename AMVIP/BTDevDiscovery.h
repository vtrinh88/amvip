//
//  BTDevDiscovery.h
//  RARMCVIOS
//
//  Created by Viet Trinh on 1/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BTDevAvailability.h"
#import "BTComService.h"

@interface BTDevDiscovery : NSObject
@property (strong, nonatomic) BTComService* btService;

+(instancetype)sharedInstance;
@end
