//
//  BTComService.m
//  RARMCVIOS
//
//  Created by Viet Trinh on 1/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import "BTComService.h"

@interface BTComService() <CBPeripheralDelegate>
@property (strong, nonatomic) CBPeripheral* peripheral;
@property (strong, nonatomic) CBCharacteristic* characteristics;
@end

@implementation BTComService

#pragma mark - LifeCycle
- (instancetype)initWithPeripheral:(CBPeripheral *)discoverdPeripheral{
    self = [super init];
    if (self) {
        self.peripheral = discoverdPeripheral;
        [self.peripheral setDelegate:self];
    }
    return self;
}

-(void)dealloc{
    [self reset];
}

#pragma mark - BTComServiceAction
- (void)reset{
    if (self.peripheral) {
        self.peripheral = nil;
    }
    [self sendBTServiceConnectivityNotification:NO];
}

- (void)startDiscoveringServices{
    [self.peripheral discoverServices:@[[CBUUID UUIDWithString:BT_SERVICE_UUID]]];
}

- (void)transmitData:(NSString*)data{
    if (!self.characteristics) {
        return;
    }
    
    NSData* dataValue = [NSData dataWithBytes:[data UTF8String] length:sizeof(data)];
    [self.peripheral writeValue:dataValue forCharacteristic:self.characteristics type:CBCharacteristicWriteWithResponse];
}

-(void)sendBTServiceConnectivityNotification:(BOOL)isPaired{
    NSDictionary* connectionInfo = @{@"isPaired":@(isPaired)};
    [[NSNotificationCenter defaultCenter] postNotificationName:BT_SERVICE_STATUS_CHANGE object:self userInfo:connectionInfo];
}

#pragma mark - CBPeripheralDelegate
-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    //NSLog(@"Discovere services");
    if (self.peripheral != peripheral || error != nil) {
        NSLog(@"Paired with incorrect peripheral device OR there is error %@",error);
        return;
    }
    
    NSArray* services = [peripheral services];
    if (![services count]) {
        NSLog(@"No found service for this peripheral");
        return;
    }
    
    for (CBService* service in services) {
        //NSLog(@"Service UUID %@",[[service UUID] UUIDString]);
        if ([[[service UUID] UUIDString] isEqualToString:BT_SERVICE_UUID]) {
            [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:BT_TX_CHARACTERISTICS_UUID]] forService:service];
        }
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    if (self.peripheral != peripheral || error != nil) {
        NSLog(@"Paired with incorrect peripheral device OR there is error %@",error);
        return;
    }
    
    NSArray* characteristics = [service characteristics];
    if (![characteristics count]) {
        NSLog(@"No found characteristic for this service");
        return;
    }
    
    for (CBCharacteristic* characteristic in characteristics) {
        //NSLog(@"Characteristic UUID %@",[[characteristic UUID] UUIDString]);
        if ([[[characteristic UUID] UUIDString] isEqualToString:BT_TX_CHARACTERISTICS_UUID]) {
            self.characteristics = characteristic;
            [self sendBTServiceConnectivityNotification:YES];
        }
    }
}

@end
