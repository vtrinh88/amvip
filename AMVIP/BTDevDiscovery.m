//
//  BTDevDiscovery.m
//  RARMCVIOS
//
//  Created by Viet Trinh on 1/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import "BTDevDiscovery.h"

@interface BTDevDiscovery() <CBCentralManagerDelegate>
@property (strong, nonatomic) CBCentralManager* centralManager;
@property (strong, nonatomic) CBPeripheral* discoveredPeripheral;
@end

@implementation BTDevDiscovery

#pragma mark - LifeCycle
+(instancetype)sharedInstance{
    static BTDevDiscovery* this = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        this = [[BTDevDiscovery alloc] init];
    });
    
    return this;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        dispatch_queue_t centralQueue = dispatch_queue_create("BTArduinoUnoDiscovery", DISPATCH_QUEUE_SERIAL);
        self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:centralQueue options:nil];
        self.btService = nil;
    }
    return self;
}

-(void)setBtService:(BTComService *)btService{
    /* Clear all previous service setups 
       for a clear new connection */
    if (_btService) {
        [_btService reset];
        _btService = nil;
    }
    
    _btService = btService;
    if (_btService) {
        [_btService startDiscoveringServices];
    }
}

-(void)starPeripheralScanning{
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:1]
                                                           forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
    [self.centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:BT_SERVICE_UUID]]
                                                options:dictionary];
    
}

-(void)clearScannedPeripheral{
    self.btService = nil;
    self.discoveredPeripheral = nil;
}

#pragma mark - CBCentralManagerDelegate
-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI{
    /* This delegate method is invoked automatically 
     once any peripheral device is discovered within its range.
     Also, the discovered peripheral is retained to avoid failure 
     or any anomaly during a connection phases*/
    
    // Make sure that the discoverd peripheral is valid
    if (!peripheral || !peripheral.name || [peripheral.name isEqualToString:@""]) {
        return;
    }
    else{
        NSLog(@"Discover name: %@", [advertisementData objectForKey:CBAdvertisementDataLocalNameKey]);
    }
    
    // Connect to this discovered peripheral if it is not paired
    if (!self.discoveredPeripheral || (self.discoveredPeripheral.state == CBPeripheralStateDisconnected)) {
        //NSLog(@"Retain a peripheral");
        self.discoveredPeripheral = peripheral;
        self.btService = nil;
        [self.centralManager connectPeripheral:peripheral options:nil];
    }
}

-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    if (self.discoveredPeripheral == peripheral) {
        self.btService = [[BTComService alloc] initWithPeripheral:peripheral];
    }
    [self.centralManager stopScan];
}

-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    if (!peripheral) {return;}
    
    if (self.discoveredPeripheral == peripheral) {
        self.discoveredPeripheral = nil;
        self.btService = nil;
    }
    
    // Scanning other devices
    [self starPeripheralScanning];

}

-(void)centralManagerDidUpdateState:(CBCentralManager *)central{
    switch (self.centralManager.state) {
        case CBCentralManagerStatePoweredOn:
            NSLog(@"Scanning bluetooth-enable devices");
            [self starPeripheralScanning];
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"Resetting bluetooth-enable devices");
            [self clearScannedPeripheral];
            break;
        case CBCentralManagerStatePoweredOff:
            NSLog(@"Powering Off");
            [self clearScannedPeripheral];
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"Unauthorized BLE devices");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"Unknown state of BLE devices");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"Unsupport BLE");
            break;
        default:
            break;
    }
}
@end
