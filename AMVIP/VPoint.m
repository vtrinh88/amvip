//
//  VPoint.m
//  AMVIP
//
//  Created by Viet Trinh on 3/8/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import "VPoint.h"

@implementation VPoint
-(instancetype)initWithXCoord:(float)XP YCoord:(float)YP ZCoord:(float)ZP{
    self = [super init];
    if (self) {
        self.x = XP;
        self.y = YP;
        self.z = ZP;
    }
    return self;
}

-(void)setXCoord:(float)XP YCoord:(float)YP ZCoord:(float)ZP{
    self.x = XP;
    self.y = YP;
    self.z = ZP;
}
@end
