//
//  Hand.cpp
//  AMVIP
//
//  Created by Viet Trinh on 2/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#include "Hand.h"
#include <cstdlib>
#include <iostream>
#include <string>
#include <cmath>
using namespace std;

Hand::Hand(){
    this->handIndex = -1;
    this->numFingers = 0;
    this->numDefects = 0;
    this->boundWidth = 0.0;
    this->boundHeight = 0.0;
}

void Hand::initHulls(){
    this->convexHullIndices = vector<vector<int>>(this->contours.size());
    this->convexHullPoints = vector<vector<cv::Point>>(this->contours.size());
    this->defects = vector<vector<Vec4i>>(this->contours.size());
}

bool Hand::isHand(){
    this->boundWidth = this->boundingBox.width;
    this->boundHeight = this->boundingBox.height;
    if (boundHeight==0 || boundWidth==0)
        return false;
    return true;
}

const int Hand::getNumberOfFingers(){
    return this->numFingers;
}

const int Hand::getNumberOfDefects(){
    return this->numDefects;
}

float Hand::distanceBetweenTwoPoints(cv::Point a, cv::Point b){
    return sqrt(fabs(pow(a.x-b.x, 2) + pow(a.y-b.y, 2)));
}

float Hand::angleBetweenLinesConnecting(cv::Point start, cv::Point defect, cv::Point end){
    cv::Point start_defect = cv::Point(start.x - defect.x, start.y - defect.y);
    cv::Point end_defect   = cv::Point(end.x - defect.x, end.y - defect.y);
    float distanceStartDefect = distanceBetweenTwoPoints(start, defect);
    float distanceEndDefect = distanceBetweenTwoPoints(end, defect);
    float dotProduct = start_defect.x*end_defect.x + start_defect.y*end_defect.y;
    float angle = acos(dotProduct/(distanceStartDefect*distanceEndDefect));
    return angle*180/PI;
}

void Hand::eliminateDefects(){
    int tolerantHeight = this->boundHeight/5;
    float tolerantAngle = 95.0;
    vector<Vec4i>tolerantDefects;
    int defectStartPtIndex, defectEndPtIndex, defectFarPtIndex;
    float defectDepth;
    vector<Vec4i>::iterator it = defects[handIndex].begin();
    while (it != defects[handIndex].end()) {
        Vec4i& defectStructure = (*it);
        defectStartPtIndex = defectStructure[0];
        defectEndPtIndex = defectStructure[1];
        defectFarPtIndex = defectStructure[2];
        defectDepth = defectStructure[3]/256.0;
        Point startPt(contours[handIndex][defectStartPtIndex]);
        Point endPt(contours[handIndex][defectEndPtIndex]);
        Point farPt(contours[handIndex][defectFarPtIndex]);
        if (distanceBetweenTwoPoints(startPt, farPt) > tolerantHeight &&
            distanceBetweenTwoPoints(endPt, farPt)   > tolerantHeight &&
            angleBetweenLinesConnecting(startPt, farPt, endPt) < tolerantAngle) {
            if (endPt.y > (boundingBox.y + boundingBox.height -boundingBox.height/4 )) {;}
            else if (startPt.y > (boundingBox.y + boundingBox.height -boundingBox.height/4 )){;}
            else if(defectDepth < tolerantHeight){;}
            else{tolerantDefects.push_back(defectStructure);}
        }
        it++;
    }
    this->numDefects = (int)tolerantDefects.size();
    defects[handIndex].swap(tolerantDefects);
    removeDefectsOverlappingFingerTips(defects[handIndex]);
}

void Hand::removeDefectsOverlappingFingerTips(std::vector<cv::Vec4i>newDefects){
    float tolerantHeight = this->boundWidth/6;
    int defectStartPt1Index, defectEndPt1Index, defectStartPt2Index, defectEndPt2Index;
    for(int i=0;i<newDefects.size();i++){
        for(int j=i;j<newDefects.size();j++){
            defectStartPt1Index=newDefects[i][0];
            defectEndPt1Index  =newDefects[i][1];
            defectStartPt2Index=newDefects[j][0];
            defectEndPt2Index  =newDefects[j][1];
            
            Point startPt1(contours[handIndex][defectStartPt1Index] );
            Point endPt1(contours[handIndex][defectEndPt1Index] );
            Point startPt2(contours[handIndex][defectStartPt2Index] );
            Point endPt2(contours[handIndex][defectEndPt2Index] );
            
            if(distanceBetweenTwoPoints(startPt1,endPt2) < tolerantHeight ){
                this->numDefects--;
                contours[handIndex][defectStartPt1Index] = endPt2;
                break;
            }if(distanceBetweenTwoPoints(endPt1,startPt2) < tolerantHeight ){
                this->numDefects--;
                contours[handIndex][defectStartPt2Index] = endPt1;
            }
        }
    }
}

void Hand::checkSingleFinger(Mat& image){
    cv::Point highestPoint;
    highestPoint.y = image.rows;
    vector<Point>::iterator it = contours[handIndex].begin();
    while(it != contours[handIndex].end()) {
        cv::Point defect = (*it);
        if(defect.y < highestPoint.y)
            highestPoint = defect;
        it++;
    }
    
    bool isSingleFinger = true;
    int tolerantHeight = boundHeight/6;
    it = convexHullPoints[handIndex].begin();
    while(it != convexHullPoints[handIndex].end()) {
        cv::Point defect = (*it);
        //cout<<"x " << v.x << " y "<<  v.y << " highestpY " << highestP.y<< "ytol "<<yTol<<endl;
        if(defect.y < highestPoint.y + tolerantHeight &&
           defect.y != highestPoint.y && defect.x != highestPoint.x){
            isSingleFinger = false;
        }
        it++;
    }
    
    if(isSingleFinger){
        fingerTips.push_back(highestPoint);
    }
}

void Hand::getFingerTips(Mat& image){
    fingerTips.clear();
    bool isFirstFinger = true;
    vector<Vec4i>::iterator it = defects[handIndex].begin();
    while( it != defects[handIndex].end() ) {
   	    Vec4i& defectStructure=(*it);
        int defectStartPtIndex = defectStructure[0];
        int defectEndPtIndex = defectStructure[1];
        int defectFarPtIndex = defectStructure[2];
        Point startPt(contours[handIndex][defectStartPtIndex]);
        Point endPt(contours[handIndex][defectEndPtIndex]);
        Point farPt(contours[handIndex][defectFarPtIndex]);
        if(isFirstFinger){
            fingerTips.push_back(startPt);
            isFirstFinger = false;
        }
        fingerTips.push_back(endPt);
        it++;
   	}
    if(fingerTips.size() == 0){
        checkSingleFinger(image);
    }
    this->numFingers = (int)fingerTips.size() - 1;
    if (this->numFingers < 0) this->numFingers = 0;
}

void Hand::drawFingerTips(Mat& image){
    for (int i=0; i<fingerTips.size(); i++)
        circle(image, fingerTips[i], 10, Scalar(0,200,0),2);
}







