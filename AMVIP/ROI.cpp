//
//  ROI.cpp
//  AMVIP
//
//  Created by Viet Trinh on 2/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#include "ROI.h"

ROI::ROI(cv::Point origin, Mat& image){
    this->origin = origin;
    this->roiPtr = image(Rect(origin.x, origin.y, ROI_SIZE, ROI_SIZE));
}

void ROI::drawRegion(cv::Mat &image){
    rectangle(image, this->origin, cv::Point(this->origin.x+ROI_SIZE,this->origin.y+ROI_SIZE), Scalar(0,255,0),STROKE_WIDTH);
}