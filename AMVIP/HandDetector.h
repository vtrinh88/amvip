//
//  HandDetector.hpp
//  AMVIP
//
//  Created by Viet Trinh on 2/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#ifndef HandDetector_hpp
#define HandDetector_hpp
#define ROI_QUANTITY    10
#define MAX_WAIT_TIME   150

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "ROI.h"
#include "Hand.h"
using namespace cv;

class HandDetector{
public:
    HandDetector();
    void scanImage(Mat& image);
    const Mat& getImageBW();
    bool isWaitingForHand();
    bool isBWImageReady();
    int numberOfFingers();
    int numberOfDefects();
    
private:
    void generateROIOrigins(Mat &image);
    void showRoisOn(Mat &image);
    void getRoisOn(Mat &image);
    void averageColor(ROI roi, int avg[3]);
    int getMedian(std::vector<int> data);
    void produceBinaries();
    void normalizeColor();
    //void printData();
    int getIndexHandContourFrom(std::vector<std::vector<cv::Point>>contours);
    void detectContours(Mat& image);
    void drawHandConvexHull(Mat& image);
    
    Mat imageScaled;
    Mat imageBW;
    std::vector<cv::Point> originList;
    std::vector<Mat> imageBWList;
    std::vector<ROI> rois;
    Hand* hand;
    int avgColor[ROI_QUANTITY][3];
    int lowColor[ROI_QUANTITY][3];
    int highColor[ROI_QUANTITY][3];
    int waitTime;
    bool isImageBWNotNull;
};

#endif /* HandDetector_hpp */
