//
//  ROI.hpp
//  AMVIP
//
//  Created by Viet Trinh on 2/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#ifndef ROI_hpp
#define ROI_hpp
#define ROI_SIZE        18
#define STROKE_WIDTH    1.0

#include <stdio.h>
#include <opencv2/opencv.hpp>
using namespace cv;

class ROI{
public:
    ROI(cv::Point origin, Mat& image);
    void drawRegion(Mat& image);
    
    cv::Point origin;
    Mat roiPtr;
};
#endif /* ROI_hpp */
