//
//  UIImage+OpenCV.mm
//  AMVIP
//
//  Created by Viet Trinh on 2/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import "UIImage+OpenCV.h"

@implementation UIImage (OpenCV)

-(Mat)toCVMat{
    return [UIImage fromUIImage:self];
}

/* Convert image from UIImage to OpenCV Matrix */
+(Mat)fromUIImage:(UIImage*)image{
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    Mat mat(rows,cols,CV_8UC4);
    
    CGContextRef contextRef = CGBitmapContextCreate(mat.data, cols, rows,
                                                    8, mat.step[0],
                                                    CGImageGetColorSpace(image.CGImage),
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault);
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return mat;
}

/* Convert image from OpenCVMatrix to UIImage */
+(UIImage*)fromCVMat:(const Mat&)mat{
    CGColorSpaceRef colorSpace = (mat.channels() == 1) ? CGColorSpaceCreateDeviceGray() : CGColorSpaceCreateDeviceRGB();
    CFDataRef imageData = CFDataCreate(kCFAllocatorDefault, mat.data, (mat.elemSize() * mat.total()));
    CGDataProviderRef provider = CGDataProviderCreateWithCFData(imageData);
    CGImageRef imageRef = CGImageCreate(mat.cols, mat.rows,
                                        8, 8*mat.elemSize(), mat.step[0],
                                        colorSpace, kCGImageAlphaNone | kCGBitmapByteOrderDefault,
                                        provider, NULL, false, kCGRenderingIntentDefault);
    
    UIImage * image = [UIImage imageWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CFRelease(imageData);
    CGColorSpaceRelease(colorSpace);
    
    return image;
}

@end
