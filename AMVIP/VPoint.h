//
//  VPoint.h
//  AMVIP
//
//  Created by Viet Trinh on 3/8/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VPoint : NSObject
@property (nonatomic) float x;
@property (nonatomic) float y;
@property (nonatomic) float z;
-(instancetype)initWithXCoord:(float)XP YCoord:(float)YP ZCoord:(float)ZP;
-(void)setXCoord:(float)XP YCoord:(float)YP ZCoord:(float)ZP;
@end
