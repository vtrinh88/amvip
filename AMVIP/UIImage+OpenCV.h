//
//  UIImage+OpenCV.h
//  AMVIP
//
//  Created by Viet Trinh on 2/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <opencv2/opencv.hpp>
using namespace cv;

@interface UIImage (OpenCV)
-(Mat)toCVMat;
+(Mat)fromUIImage:(UIImage*)image;
+(UIImage*)fromCVMat:(const Mat&)mat;
@end
