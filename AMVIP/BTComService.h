//
//  BTComService.h
//  RARMCVIOS
//
//  Created by Viet Trinh on 1/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BTDevAvailability.h"

@interface BTComService : NSObject
- (instancetype)initWithPeripheral:(CBPeripheral *)discoverdPeripheral;
- (void)reset;
- (void)startDiscoveringServices;
- (void)transmitData:(NSString*)data;
@end
