//
//  Manipulator.m
//  AMVIP
//
//  Created by Viet Trinh on 3/8/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import "Manipulator.h"

@interface Manipulator()
@property (nonatomic) VPoint* tip;
@property (nonatomic) float theta1;
@property (nonatomic) float theta2;
@property (nonatomic) float theta3;
@property (nonatomic) float L1;
@property (nonatomic) float L2;
@property (nonatomic) float L3;
@end

@implementation Manipulator
-(instancetype)initWithL1:(float)L1 L2:(float)L2 L3:(float)L3{
    self = [super init];
    if (self) {
        self.tip = [[VPoint alloc] initWithXCoord:0.0 YCoord:0.0 ZCoord:0.0];
        self.theta1 = 0.0;
        self.theta2 = 0.0;
        self.theta3 = 0.0;
        self.L1 = L1;
        self.L2 = L2;
        self.L3 = L3;
    }
    return self;
}

-(NSArray*)IKJointAnglesFor:(int)number{
    [self setTipLocationFor:number];
    float r1 = sqrtf(pow(self.tip.x, 2.0) + pow(self.tip.y, 2.0));
    float r2 = sqrtf(pow(r1, 2.0) + pow(self.tip.z - self.L1, 2.0));
    self.theta1 = atan2f(self.tip.y, self.tip.x);
    self.theta2 = atan2f(self.tip.z-self.L1, r1) +
                  atan2f(sqrtf(4*pow(self.L2, 2.0)*pow(r2, 2.0) - pow(pow(self.L2, 2.0)+pow(r2, 2.0)-pow(self.L3, 2.0), 2.0)),
                         (pow(self.L2, 2.0)+pow(r2, 2.0)-pow(self.L3, 2.0)));
    self.theta3 = atan2f(sqrtf(4*pow(self.L2, 2.0)*pow(self.L3, 2.0) - pow(pow(r2, 2.0)-pow(self.L2, 2.0)-pow(self.L3, 2.0), 2.0)),
                        (pow(r2, 2.0)-pow(self.L2, 2.0)-pow(self.L3, 2.0)));
    
    return @[[NSNumber numberWithFloat:self.theta1],
             [NSNumber numberWithFloat:self.theta2],
             [NSNumber numberWithFloat:self.theta3]];
}

-(void)setTipLocationFor:(int)number{
    VPoint* currentTipLocation = self.tip;
    // performing calculation to determine new ee location
    // code here
    int newNumber = number;
    float x, y, z;
    
    [self.tip setXCoord:x YCoord:y ZCoord:z];
}
@end



