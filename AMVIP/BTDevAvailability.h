//
//  BTDevAvailability.h
//  RARMCVIOS
//
//  Created by Viet Trinh on 1/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#ifndef BTDevAvailability_h
#define BTDevAvailability_h

/* Services & Characteristics UUIDs */
#define BT_SERVICE_UUID             @"B4BDB998-8F4A-45F6-A407-6B48D79CFC2F"
#define BT_TX_CHARACTERISTICS_UUID  @"B4BDB998-8F4E-45F6-A407-6B48D79CFC2F"
#define BT_SERVICE_STATUS_CHANGE    @"BTServiceStatusChangeNotification"

/*
 Services and Characteristics for SeedStudio BLE Shield V3.0
 
 A. Service:
    BLE-Shield Service v3.0.0: B4BDB998-8F4A-45F6-A407-6B48D79CFC2F
 
 B. Characteristics:
 
 1. Bluetooth Device Address (Read): B4BDB998-8F4B-45F6-A407-6B48D79CFC2F
 
 2. Baudrate (read, write): B4BDB998-8F4C-45F6-A407-6B48D79CFC2F
    Supported baudrate values are:
    0x00 = 9600 Baud (default)
    0x01 = 14400 Baud
    0x02 = 19200 Baud
    0x03 = 28800 Baud
    0x04 = 38400 Baud
    0x05 = 57600 Baud
    0x06 = 112500 Baud
 
    The baudrate setting is written to the internal storage of the BLE113 module, so the setting will persist even if you remove power from the module.
    During the next boot cycle after power cycling the module or resetting it, the baudrate setting is restored from flash. So there is no need to 
    reconfigure the BLE-Shield every time you power the module.
 
 3. Enable Connect LED (read, write): B4BDB998-8F4D-45F6-A407-6B48D79CFC2F
    This new characteristic is used to change the bahavior of the blue LED on the BLE-Shield which indicates the connection status of the BLE-Shield.
    Supported values are:
        0x00 = LED is not used to indicate a connection (Default)
        0xFF = LED is used to indicate a connection.
    So don’t worry if the LED is not turned on while connected if you are using the BLE-Shield the first time. It is turned off by default. You need to
    change the characteristic first to 0xFF in order to use the LED. The setting is also persisted into the BLE-Shield’s flash memory, so the setting
    persists between power and reset cycles.
 
 4. Data (Write, Indicate): B4BDB998-8F4E-45F6-A407-6B48D79CFC2F
    The Data characteristic is new to the BLE-Shield v3.0.0. If you connect the BLE-Shield from e.g. an iOS Device and you enable indications on this 
    characteristic, the iOS device will be indicated whenever the embedded device sends serial data to the BLE-Shield on the TX pin. If you write data from
    the iOS device to the BLE-Shield the data is send available on the RX pin of the BLE-Shield.
**/

#endif /* BTDevAvailability_h */
