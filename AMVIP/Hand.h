//
//  Hand.hpp
//  AMVIP
//
//  Created by Viet Trinh on 2/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#ifndef Hand_hpp
#define Hand_hpp
#define PI 3.1415

#include <stdio.h>
#include <opencv2/opencv.hpp>
using namespace cv;

class Hand{
public:
    Hand();
    void initHulls();
    bool isHand();
    void eliminateDefects();
    void getFingerTips(Mat& image);
    void drawFingerTips(Mat& image);
    const int getNumberOfFingers();
    const int getNumberOfDefects();
    
    int handIndex;
    cv::Rect boundingBox;
    std::vector<std::vector<cv::Point>>contours;
    std::vector<std::vector<int>>convexHullIndices;
    std::vector<std::vector<cv::Point>>convexHullPoints;
    std::vector<std::vector<cv::Vec4i>>defects;
    std::vector<cv::Point>fingerTips;
    
private:
    int numFingers;
    int numDefects;
    double boundWidth;
    double boundHeight;
    float distanceBetweenTwoPoints(cv::Point a, cv::Point b);
    float angleBetweenLinesConnecting(cv::Point start, cv::Point defect, cv::Point end);
    void removeDefectsOverlappingFingerTips(std::vector<cv::Vec4i>newDefects);
    void checkSingleFinger(Mat& image);
};

#endif /* Hand_hpp */
