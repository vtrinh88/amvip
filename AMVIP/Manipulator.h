//
//  Manipulator.h
//  AMVIP
//
//  Created by Viet Trinh on 3/8/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VPoint.h"

@interface Manipulator : NSObject
-(instancetype)initWithL1:(float)L1 L2:(float)L2 L3:(float)L3;
-(NSArray*)IKJointAnglesFor:(int)number;
@end
