//
//  HandDetector.cpp
//  AMVIP
//
//  Created by Viet Trinh on 2/17/16.
//  Copyright © 2016 Viet Trinh. All rights reserved.
//

#include "HandDetector.h"
#include <cstdlib>
#include <iostream>
#include <string>
#include <cmath>
using namespace std;

HandDetector::HandDetector(){
    this->waitTime = 0;
    this->isImageBWNotNull = false;
    this->hand = new Hand();
}

void HandDetector::scanImage(Mat& image){
    if(isWaitingForHand()){
        showRoisOn(image);
        if (!isWaitingForHand()) {
            getRoisOn(image);
            //printData();
        }
    }
    else{
        pyrDown(image, this->imageScaled);
        blur(this->imageScaled, this->imageScaled, Size(3,3));
        medianBlur(this->imageScaled, this->imageScaled,9);
        produceBinaries();
        // clear up memory for vector
        vector<ROI>().swap(this->rois);
        vector<Mat>().swap(this->imageBWList);
        detectContours(image);
    }
}

const Mat& HandDetector::getImageBW(){
    return this->imageBW;
}

bool HandDetector::isWaitingForHand(){
    return (this->waitTime <= MAX_WAIT_TIME) ? true : false;
}

bool HandDetector::isBWImageReady(){
    return this->isImageBWNotNull;
}

void HandDetector::generateROIOrigins(Mat& image){
    if (this->originList.size()<ROI_QUANTITY) {
        int cols = image.cols;
        int rows = image.rows;
        this->originList.push_back(cv::Point(3*cols/8,rows/2));
        this->originList.push_back(cv::Point(7*cols/16,5*rows/16));
        this->originList.push_back(cv::Point(cols/2,rows/4));
        this->originList.push_back(cv::Point(9*cols/16,5*rows/16));
        this->originList.push_back(cv::Point(5*cols/8,3*rows/8));
        this->originList.push_back(cv::Point(cols/2,rows/2));
        this->originList.push_back(cv::Point(7*cols/16,5*rows/8));
        this->originList.push_back(cv::Point(9*cols/16,5*rows/8));
        this->originList.push_back(cv::Point(7*cols/16,6*rows/8));
        this->originList.push_back(cv::Point(9*cols/16,6*rows/8));
    }
}

void HandDetector::showRoisOn(Mat &image){
    generateROIOrigins(image);
    if (this->rois.size() < ROI_QUANTITY)
        for (int i=0; i< ROI_QUANTITY; i++)
            this->rois.push_back(ROI(this->originList[i], image));
    
    for (int i=0; i< ROI_QUANTITY; i++)
        this->rois[i].drawRegion(image);
    
    this->waitTime++;
}

void HandDetector::getRoisOn(Mat &image){
    for (int i=0; i<ROI_QUANTITY; i++) {
        averageColor(this->rois[i],this->avgColor[i]);
        this->rois[i].drawRegion(image);
    }
}

void HandDetector::averageColor(ROI roi, int avg[3]){
    Mat lroi = roi.roiPtr;
    //roi.roiPtr.copyTo(lroi);
    cvtColor(lroi, lroi, CV_BGR2HLS);
    vector<int>hchannel;
    vector<int>schannel;
    vector<int>lchannel;
    for(int r=0; r<lroi.rows; r++){
        for(int c=0; c<lroi.cols; c++){
            hchannel.push_back(lroi.data[lroi.channels()*(lroi.cols*r + c) + 0]) ;
            schannel.push_back(lroi.data[lroi.channels()*(lroi.cols*r + c) + 1]) ;
            lchannel.push_back(lroi.data[lroi.channels()*(lroi.cols*r + c) + 2]) ;
        }
    }
    avg[0]=getMedian(hchannel);
    avg[1]=getMedian(schannel);
    avg[2]=getMedian(lchannel);
    cvtColor(lroi, lroi, CV_HLS2BGR);
}

int HandDetector::getMedian(std::vector<int> data){
    size_t size = data.size();
    sort(data.begin(), data.end());
    return (size%2 == 0) ? data[size/2 - 1] : data[size/2];
}

void HandDetector::produceBinaries(){
    Scalar lowerBound;
    Scalar upperBound;
    cvtColor(this->imageScaled, this->imageScaled, CV_BGR2HLS);
    for (int i=0; i<ROI_QUANTITY; i++) {
        normalizeColor();
        lowerBound = Scalar(avgColor[i][0]-lowColor[i][0],avgColor[i][1]-lowColor[i][1],avgColor[i][2]-lowColor[i][2]);
        upperBound = Scalar(avgColor[i][0]+highColor[i][0],avgColor[i][1]+highColor[i][1], avgColor[i][2]+highColor[i][2]);
        this->imageBWList.push_back(Mat(this->imageScaled.rows,this->imageScaled.cols,CV_8U));
        inRange(this->imageScaled,lowerBound,upperBound,this->imageBWList[i]);
    }
    this->imageBWList[0].copyTo(this->imageBW);
    for(int i=1;i<ROI_QUANTITY;i++){
        this->imageBW += this->imageBWList[i];
    }
    medianBlur(this->imageBW, this->imageBW, 9);
    this->isImageBWNotNull = true;
}

void HandDetector::normalizeColor(){
    for(int i=0;i<ROI_QUANTITY;i++){
        this->lowColor[i][0]=12;
        this->highColor[i][0]=7;
        this->lowColor[i][1]=30;
        this->highColor[i][1]=40;
        this->lowColor[i][2]=80;
        this->highColor[i][2]=80;
    }
    
    for(int i=0;i<ROI_QUANTITY;i++){
        for (int j=0; j<3; j++) {
            if((this->avgColor[i][j] - this->lowColor[i][j]) < 0)
                this->lowColor[i][j] = this->avgColor[i][j];
            if((this->avgColor[i][j] + this->highColor[i][j]) > 255)
                this->highColor[i][j] = 255 - this->avgColor[i][j] ;
        }
    }
}

/*
 void HandDetector::printData(){
    for (int i=0; i<ROI_QUANTITY; i++) {
        cout << "["<<avgColor[i][0]<<","<<avgColor[i][1]<<","<<avgColor[i][2]<<"]"<<endl;
    }
}
*/

int HandDetector::getIndexHandContourFrom(std::vector<std::vector<cv::Point>>contours){
    int largestContourIndex = -1;
    int largestContourSize = 0;
    
    for (int i=0; i<contours.size(); i++) {
        if (contours[i].size() > largestContourSize) {
            largestContourSize = (int)contours[i].size();
            largestContourIndex = i;
        }
    }
    
    return largestContourIndex;
}

void HandDetector::detectContours(Mat& image){
    Mat binaryImg;
    this->imageBW.copyTo(binaryImg);
    pyrUp(binaryImg, binaryImg);
    findContours(binaryImg, hand->contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
    hand->initHulls();
    hand->handIndex = getIndexHandContourFrom(hand->contours);
    if (hand->handIndex != -1) {
        hand->boundingBox = boundingRect(Mat(hand->contours[hand->handIndex]));
        convexHull(Mat(hand->contours[hand->handIndex]),hand->convexHullPoints[hand->handIndex],false,true);
        convexHull(Mat(hand->contours[hand->handIndex]),hand->convexHullIndices[hand->handIndex],false,false);
        approxPolyDP(Mat(hand->convexHullPoints[hand->handIndex]), hand->convexHullPoints[hand->handIndex], 18, true );
        if(hand->contours[hand->handIndex].size()>3 ){
            convexityDefects(hand->contours[hand->handIndex],hand->convexHullIndices[hand->handIndex],hand->defects[hand->handIndex]);
            
            hand->eliminateDefects();
        }
        if (hand->isHand()) {
            hand->getFingerTips(image);
            hand->drawFingerTips(image);
            drawHandConvexHull(image);
        }
    }
}

void HandDetector::drawHandConvexHull(Mat& image){
    drawContours(image, hand->convexHullPoints, hand->handIndex, Scalar(255,0,0),2,8,vector<Vec4i>(),0,cv::Point());
    //rectangle(image, hand->boundingBox.tl(), hand->boundingBox.br(), Scalar(0,0,200));
}

int HandDetector::numberOfFingers(){
    return (hand!= NULL) ? hand->getNumberOfFingers() : 0;
}

int HandDetector::numberOfDefects(){
    return (hand!= NULL) ? hand->getNumberOfDefects() : 0;
}










